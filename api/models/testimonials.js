const mongoose = require('mongoose');

const testimonialSchema = mongoose.Schema({
    _id: mongoose.Schema.Types.ObjectId,
    name: { type: String, required: true },
    message: { type: String, required: true },
    designation: { type: String, required: true },
    image: { type: String, required: true }
});

module.exports = mongoose.model('Testimonials', testimonialSchema);