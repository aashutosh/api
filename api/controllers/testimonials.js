const mongoose = require("mongoose");
const Testimonials = require("../models/testimonials");

const multer = require('multer');

exports.testimonials_get_all = (req, res, next) => {
    Testimonials.find()
        .exec()
        .then(docs => {
            const response = {
                count: docs.length,
                testimonials: docs.map(doc => {
                    return {
                        name: doc.name,
                        message: doc.message,
                        designation: doc.designation,
                        _id: doc._id,
                        image: doc.image,

                    };
                })
            };
            //   if (docs.length >= 0) {
            res.status(200).json(response);
            //   } else {
            //       res.status(404).json({
            //           message: 'No entries found'
            //       });
            //   }
        })
        .catch(err => {
            console.log(err);
            res.status(500).json({
                error: err
            });
        });
};
exports.testimonials_add = (req, res, next) => {
    const testimonials = new Testimonials({
        _id: new mongoose.Types.ObjectId(),
        name: req.body.name,
        message: req.body.message,
        designation: req.body.designation,
        image: req.file.path
    });
    testimonials
        .save()
        .then(result => {
            console.log(result);
            res.status(201).json({
                message: "Added  successfully",
                info: {
                    name: result.name,
                    image: result.image,
                    message: result.message,
                    designation: result.designation,
                    _id: result._id,

                }
            });
        })
        .catch(err => {
            console.log(err);
            res.status(500).json({
                error: err
            });
        });


};

exports.testimonials_get = (req, res, next) => {
    const id = req.params.testimonialsId;
    Testimonials.findById(id)
        .exec()
        .then(doc => {
            if (doc) {
                res.status(200).json({
                    product: doc,
                    request: {
                        type: "GET",
                        url: "http://localhost:3000/testimonials"
                    }
                });
            } else {
                res
                    .status(404)
                    .json({ message: "No valid entry found for provided ID" });
            }
        })
        .catch(err => {
            console.log(err);
            res.status(500).json({ error: err });
        });
};

exports.testimonials_delete = (req, res, next) => {
    const id = req.params.testimonialsId;
    Testimonials.remove({ _id: id })
        .exec()
        .then(result => {
            res.status(200).json({
                message: "Deleted Testimonials",

            });
        })
        .catch(err => {
            console.log(err);
            res.status(500).json({
                error: err
            });
        });
};

