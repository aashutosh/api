const mongoose = require("mongoose");
const Partners = require("../models/partners");

const multer = require('multer');

exports.partners_get_all = (req, res, next) => {
    Partners.find()
    .exec()
    .then(docs => {
      const response = {
        count: docs.length,
        partners: docs.map(doc => {
          return {
            name: doc.name,
            _id: doc._id,
            image: doc.image,
            
          };
        })
      };
      //   if (docs.length >= 0) {
      res.status(200).json(response);
      //   } else {
      //       res.status(404).json({
      //           message: 'No entries found'
      //       });
      //   }
    })
    .catch(err => {
      console.log(err);
      res.status(500).json({
        error: err
      });
    });
};
exports.partners_add = (req, res, next) => {
    const partners = new Partners({
        _id: new mongoose.Types.ObjectId(),
        name: req.body.name,
        image: req.file.path
    });
    partners
        .save()
        .then(result => {
            console.log(result);
            res.status(201).json({
                message: "Added partners successfully",
                partnersInfo: {
                    name: result.name,
                    image:result.image,
                    _id: result._id,

                }
            });
        })
        .catch(err => {
            console.log(err);
            res.status(500).json({
                error: err
            });
        });


};
exports.partners_delete = (req, res, next) => {
     const id = req.params.partnerId;
  Partners.remove({ _id: id })
    .exec()
    .then(result => {
      res.status(200).json({
        message: "User deleted",

      });
    })
    .catch(err => {
      console.log(err);
      res.status(500).json({
        error: err
      });
    });
};

exports.partners_update = (req, res, next) => {
    const id = req.params.partnerId;
    res.json({
        message: 'Updated'
    });
};
