const express = require("express");
const router = express.Router();
const multer = require('multer');
const checkAuth = require('../middleware/check-auth');
const PartnersControllers = require('../controllers/partnersController');

const storage = multer.diskStorage({
    destination: (req, file, callback) => {
        callback(null, './uploads');
    },
    filename: (req, file, callback) => {
        callback(null, Date.now() + '-' + file.originalname);
    }
});
const fileFilter = (req, file, cb) => {
    // reject a file
    if (file.mimetype === 'image/jpeg' || file.mimetype === 'image/png') {
        cb(null, true);
    } else {
        cb(null, false);
    }
};

const upload = multer({
    storage: storage,
    limits: {
        fileSize: 1024 * 1024 * 5
    },
    fileFilter: fileFilter
});
router.get("/", PartnersControllers.partners_get_all);

router.post("/", upload.single('image'), PartnersControllers.partners_add);

router.delete("/:partnerId", PartnersControllers.partners_delete);

router.put("/:partnerId", PartnersControllers.partners_update);

module.exports = router;